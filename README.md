#### BUILDING

You need to install rustc and cargo from [the Rust Programming Language](https://www.rust-lang.org/en-US/install.html) 

Then run:

    cargo install --git https://sala6@bitbucket.org/sala6/sifter.git


#### USING FOR SIF to JSON (ITMprobe)

    sifter json input.sif > output.json


#### USING FOR JSON TO SIF

    sifter sif input.json > output.sif
